class EventsController < ApplicationController
  before_filter :authenticate
  before_filter :correct_user

  def new
    @title = "New Event"
    @event = Event.new
  end

  def index
    @schedule = Schedule.find(params[:schedule_id])
    @events = @schedule.events.paginate(:page => params[:page], :per_page => 10)
    @title = "Events for " + @schedule.title
  end

  def create
    @schedule = Schedule.find(params[:schedule_id])
    @event = @schedule.events.build(params[:event])
    if @event.save
      flash[:success] = "Event Created!"
      redirect_to @schedule
    else
      @title = "New Event"
      render 'new'
    end
  end

  def edit
    @event = Event.find(params[:id])
  end

  def show
    @event = Event.find(params[:id])
  end

  def destroy
    @event = Event.find(params[:id])
    @schedule = @event.schedule
    if @event.destroy
      flash[:success] = 'Event Deleted!'
      redirect_to schedule_events_url(@schedule)
    else
      flash[:error] = 'Error deleting schedule.'
      render @event
    end
  end

  def update
    @event = Event.find(params[:id])
    if @event.update_attributes(params[:event])
      flash[:success] = "Event updated."
      redirect_to @event
    else
      flash[:failure] = "Update failed."
      @title = "Edit event"
      render 'edit'
    end

  end

  private

  def authenticate
    deny_access unless signed_in?
  end

  def correct_user
    #check needed because of shallow nesting resource routes
    if params.has_key?(:schedule_id) then
      @schedule = Schedule.find(params[:schedule_id])
    else
      @schedule = Event.find(params[:id]).schedule
    end
    redirect_to(root_path) unless current_user?(@schedule.user)
  end

end
