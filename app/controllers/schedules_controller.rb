class SchedulesController < ApplicationController
  before_filter :authenticate
  before_filter :correct_user, :only => [:show, :edit, :update, :destroy]
  
  def new
    @schedule = Schedule.new()
    @title = "New Schedule"
  end
  
  def index
    @title = "Your Schedules"
    @schedules = current_user.schedules.paginate(:page => params[:page])
  end
  
  def show
    @schedule = Schedule.find(params[:id])
    @title = @schedule.title
    @events = @schedule.events.paginate(:page => params[:page], :per_page => 10)
  end
  
  def edit
    @schedule = Schedule.find(params[:id])
  end
  
  def update
    @schedule = Schedule.find(params[:id])
    if @schedule.update_attributes(params[:schedule])
      flash[:success] = "Schedule updated."
      redirect_to @schedule
    else
      @title = "Edit schedule"
      render 'edit'
    end
  end
  
  def create
    @schedule = current_user.schedules.build(params[:schedule])
    if @schedule.save
      flash[:success] = "Schedule Created!"
      redirect_to @schedule
    else
      flash[:error] = "Error creating schedule."
      render 'new'
    end
  end
  
  def destroy
    @schedule = Schedule.find(params[:id])
    if @schedule.destroy
      flash[:success] = "Schedule Deleted!"
        redirect_to :action =>  :index
    else
      flash[:error] = "Error deleting schedule."
      render @schedule
    end
    
  end
  
  private
  
  
  def correct_user
    @schedule = Schedule.find(params[:id])
    unless current_user.id == @schedule.user_id
      redirect_to(root_path)
    end
  end

end
