class UsersController < ApplicationController
  before_filter :authenticate,      :only => [:index, :show, :edit, :update, :destroy]
  before_filter :correct_or_admin,  :only => [:show, :edit, :update]
  before_filter :admin_user,        :only => [:index, :destroy]

  def new
    @user = User.new
    @title = "sign up"
  end

  def index
    @title = "All users"
    @users = User.paginate(:page => params[:page], :per_page => params[:per_page])
  end

  def show
    @user = User.find(params[:id])
    @schedules = @user.schedules.paginate(:page => params[:page])
    @title = @user.name
  end

  def create
    @user = User.new(params[:user])
    if @user.save
      sign_in @user
      flash[:success] = "Welcome to halftime!"
      redirect_to @user
    else
      @title = "sign up"
      render 'new'
    end
  end

  def edit
    @title = "Edit user"
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(params[:user])
      flash[:success] = "Profile updated."
      redirect_to @user
    else
      @title = "Edit user"
      render 'edit'
    end
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:succcess] = "User destroyed."
    redirect_to users_path
  end

  private

  def correct_or_admin
    @user = User.find(params[:id])
    redirect_to(root_path) unless (current_user.admin? or current_user?(@user))
  end

  def correct_user
    @user = User.find(params[:id])
    redirect_to(root_path) unless current_user?(@user)
  end

  def admin_user
    redirect_to(root_path) unless current_user.admin?
  end

end
