module ApplicationHelper
  
  # Automatically customizes the title
  def title
    base_title = "halftime"
    if @title.nil?
      base_title
    else
      "#{base_title} | #{@title}"
    end
  end
  
  def logo
    image_tag("logo.png", :alt => "halftime", :class => "round")
  end
end
