class Event < ActiveRecord::Base
  attr_accessible :description, :occursAt
  
  belongs_to :schedule
  
  validates :description, :presence => true, :length => { :maximum => 140 }
  validates :schedule_id, :presence => true
  validates :occursAt, :presence => true
  
  default_scope :order => 'events.occursAt ASC'
end
