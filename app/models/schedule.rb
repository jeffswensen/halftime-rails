class Schedule < ActiveRecord::Base
  attr_accessible :title
  has_many :events, :dependent => :destroy
  belongs_to :user
  
  validates :title, :presence => true, :length => { :maximum => 140 }
  validates :user_id, :presence => true
  
  default_scope :order => 'schedules.created_at DESC'
end
