class CreateSchedules < ActiveRecord::Migration
  def self.up
    create_table :schedules do |t|
      t.string :title
      t.integer :user_id

      t.timestamps
    end
    add_index :schedules, :user_id
    add_index :schedules, :created_at
  end

  def self.down
    drop_table :schedules
  end
end
