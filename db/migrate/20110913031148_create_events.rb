class CreateEvents < ActiveRecord::Migration
  def self.up
    create_table :events do |t|
      t.string :description
      t.datetime :occursAt
      t.integer :schedule_id

      t.timestamps
    end
    
    add_index :events, :schedule_id
    add_index :events, :occursAt
  end

  def self.down
    drop_table :events
  end
end
