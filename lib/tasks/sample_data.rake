namespace :db do
  desc "Fill database with sample data"
  task :populate => :environment do
    Rake::Task['db:reset'].invoke
    admin = User.create!(:name => "Example User",
                  :email => "admin@halftime.com",
                  :password => "foobar",
                  :password_confirmation => "foobar")
    admin.toggle!(:admin)
    regular_user = User.create!(:name => "Regular User",
                  :email => "regular@halftime.com",
                  :password => "foobar",
                  :password_confirmation => "foobar")
    99.times do |n|
      name = Faker::Name.name
      email = "example-#{n+1}@halftime.com"
      password = "password"
      User.create!(:name => name,
                   :email => email,
                   :password => password,
                   :password_confirmation => password)
    end
    r = Random.new
    User.all(:limit => 6).each do |user|
      50.times do
        @schedule = user.schedules.create!(:title => Faker::Lorem.sentence(5))
        20.times do
          @schedule.events.create!(:description => Faker::Lorem.sentence(2),
                                   :occursAt => r.rand(1...90).days.from_now)
        end
      end
    end
  end
end