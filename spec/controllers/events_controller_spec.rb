require 'spec_helper'

describe EventsController do
  describe "GET 'new'" do
    let(:user) { Factory(:user) }
    let(:schedule) { Factory(:schedule, :user => user) }
    let(:action) { get 'new', :schedule_id => schedule }

    it_should_require "sign in" do
      let(:expected_result) { be_success }
    end

    it_should_require "correct user" do
      let(:expected_result) { be_success }
    end

    it "should render the new template" do
      test_sign_in(user)
      action
      expect(response).to render_template("new")
    end

    it "should set the @title variable" do
      test_sign_in(user)
      action
      expect(assigns(:title))
    end

    it "should set the @title variable to 'New Event'" do
      test_sign_in(user)
      action
      expect(assigns(:title)).to eq("New Event")
    end
  end

  describe "GET 'index'" do

    let(:user) { Factory(:user)}
    let(:schedule) { Factory(:schedule, :user => user)}
    let(:event) { Factory(:event, :schedule => schedule)}
    let(:action) {get 'index', :schedule_id => schedule}

    it_should_require "sign in" do
      let(:expected_result) {be_success}
    end

    it_should_require "correct user" do
      let(:expected_result) {be_success}
    end

    it "should render the index template" do
      test_sign_in(user)
      action
      expect(response).to render_template("index")
    end

    it "should set the @title variable" do
      test_sign_in(user)
      action
      expect(assigns(:title))
    end

    it "should set the @title variable to 'Events for Schedule.title'" do
      test_sign_in(user)
      action
      expect(assigns(:title)).to eq("Events for #{schedule.title}")
    end
  end

  describe "POST 'create'" do

    let(:user) {Factory(:user)}
    let(:schedule) {Factory(:schedule, :user => user)}
    let(:attr) {{:description => 'New Event Description', :occursAt => 3.days.from_now}}
    let(:action) {post 'create', :schedule_id => schedule, :event => attr}

    it_should_require "sign in" do
      let(:expected_result) {redirect_to(schedule)}
    end

    it_should_require "correct user" do
      let(:expected_result) {redirect_to(schedule)}
    end


    it "should create a new event" do
      test_sign_in(user)
      expect { action }.to change(schedule.events, :count).by(1)
    end
  end



  describe "GET 'edit'" do
    let(:user) { Factory(:user) }
    let(:schedule) { Factory(:schedule, :user => user) }
    let(:event) { Factory(:event, :schedule => schedule) }
    let(:action) { get 'edit', :id => event.id }

    it_should_require "sign in" do
      let(:expected_result) { be_success }
    end

    it_should_require "correct user" do
      let(:expected_result) { be_success }
    end

    it "should set the @event variable" do
      test_sign_in(user)
      action
      expect(assigns(:event)).to eq(event)
    end

    it "should render the 'edit' template" do
      test_sign_in(user)
      action
      expect(response).to render_template('edit')
    end
  end

  describe "GET 'show'" do

    let(:user) { Factory(:user) }
    let(:schedule) { Factory(:schedule, :user => user) }
    let(:event) { Factory(:event, :schedule => schedule) }
    let(:action) { get 'show', :schedule_id => schedule, :id => event }

    it_should_require "sign in" do
      let(:expected_result) { be_success }
    end

    it_should_require "correct user" do
      let(:expected_result) { be_success }
    end

    it "should render the show template" do
      test_sign_in(user)
      action
      expect(response).to render_template("show")
    end

    it "should set the @event variable" do
      test_sign_in(user)
      action
      expect(assigns(:event))
    end

    it "should set the @event.description variable to 'Example Event'" do
      test_sign_in(user)
      action
      expect(event.description).to eq("Example Event")
    end
  end

  describe "POST 'update'" do

    let(:user) { Factory(:user) }
    let(:schedule) { Factory(:schedule, :user => user) }
    let(:event) { Factory(:event, :schedule => schedule) }
    let(:attr) { {:description => "New Description", :occursAt => 3.weeks.from_now } }
    let(:action) { post :update, :schedule_id => schedule, :id => event, :event => attr }

    it_should_require "sign in" do
      let(:expected_result) { redirect_to(event) }
    end

    it_should_require "correct user" do
      let(:expected_result) { redirect_to(event) }
    end

    it "should update the saved event" do
      test_sign_in(user)
      action
      event.reload
      expect(event.description).to eq("New Description")
    end
  end

  describe "DELETE 'destroy'" do
    let!(:user) { Factory(:user) }
    let!(:schedule) { Factory(:schedule, :user => user) }
    let!(:event) { Factory(:event, :schedule => schedule) }
    let(:action) { delete 'destroy', :id => event }

    it_should_require "sign in" do
      let(:expected_result) { redirect_to(schedule_events_path(schedule)) }
    end

    it_should_require "correct user" do
      let(:expected_result) { redirect_to(schedule_events_path(schedule))}
    end

    it "should delete the event" do
      test_sign_in(user)
      expect{action}.to change{Event.count}.by(-1)
    end

  end

end
