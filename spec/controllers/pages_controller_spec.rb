require 'spec_helper'

describe PagesController do
  describe "GET 'home'" do
    let(:action) { get 'home' }
    it "should be successful" do
      expect(action).to be_success
    end

    it "should set the @title variable to 'home'" do
      action
      expect(assigns(:title)).to eq("home")
    end
  end

  describe "GET 'contact'" do
    let(:action) { get 'contact' }
    it "should be successful" do
      expect(response).to be_success
    end

    it "should set the @title variable to 'contact'" do
      action
      expect(assigns(:title)).to eq("contact")
    end
  end

  describe "GET 'about'" do
    let(:action) { get 'about' }
    it "should be successful" do
      expect(response).to be_success
    end

    it "should set the @title variable to 'about'" do
      action
      expect(assigns(:title)).to eq("about")
    end
  end
end
