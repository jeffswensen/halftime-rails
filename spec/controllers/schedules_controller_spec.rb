require 'spec_helper'

describe SchedulesController do
  describe "GET 'new'" do
    let(:user) { Factory(:user) }
    let(:action) { get 'new' }

    it_should_require "sign in" do
      let(:expected_result) { be_success }
    end

    it "should set the @title variable" do
      test_sign_in(user)
      action
      expect(assigns(:title))
    end

    it "should set the @title variable to 'New Schedule'" do
      test_sign_in(user)
      action
      expect(assigns(:title)).to eq("New Schedule")
    end
  end

  describe "GET 'index'" do
    let(:user) { Factory(:user) }
    let(:action) { get :index }

    it_should_require "sign in" do
      let(:expected_result) { be_success }
    end

    it "should set the @title variable" do
      test_sign_in(user)
      action
      expect(assigns(:title))
    end

    it "should set the @title variable to 'Your Schedules'" do
      test_sign_in(user)
      action
      expect(assigns(:title)).to eq("Your Schedules")
    end

    it "should render the index template" do
      test_sign_in(user)
      action
      expect(response).to render_template("index")
    end
  end

  describe "POST 'create'" do
    let(:user) { Factory(:user) }
    let(:attr) { {:title => 'New Schedule Title'} }
    let(:action) { post 'create', :schedule => attr}

    it_should_require "sign in" do
      let(:expected_result) { redirect_to(Schedule.last) }
      # could also do redirect_to(assigns(:schedule))
    end

    it "should create a new schedule" do
      test_sign_in(user)
      action
      expect(assigns(:schedule)).to be_a(Schedule)
    end
  end

  describe "GET 'edit'" do
    let(:user) { Factory(:user) }
    let(:schedule) { Factory(:schedule, :user => user) }
    let(:action) { get :edit, :id => schedule }

    it_should_require "sign in" do
      let(:expected_result) { be_success }
    end

    it_should_require "correct user" do
      let(:expected_result) { be_success }
    end

    it "should set the @schedule variable" do
      test_sign_in(user)
      action
      expect(assigns(:schedule)).to eq(schedule)
    end

    it "should render the 'edit' template" do
      test_sign_in(user)
      action
      expect(response).to render_template('edit')
    end
  end

  describe "GET 'show'" do
    let(:user) { Factory(:user) }
    let(:schedule) { Factory(:schedule, :user => user) }
    let(:action) { get 'show', :id => schedule }

    it_should_require "sign in" do
      let(:expected_result) { be_success }
    end

    it_should_require "correct user" do
      let(:expected_result) { be_success }
    end

    it "should render the show template" do
      test_sign_in(user)
      action
      expect(response).to render_template("show")
    end

    it "should set the @schedule variable" do
      test_sign_in(user)
      action
      expect(assigns(:schedule)).to eq(schedule)
    end
  end

  describe "POST 'update'" do
    let(:user) { Factory(:user) }
    let(:schedule) { Factory(:schedule, :user => user) }
    let(:attr) { {:title => "New Title"} }
    let(:action) { post :update, :id => schedule, :schedule => attr }

    it_should_require "sign in" do
      let(:expected_result) { redirect_to(schedule) }
    end

    it_should_require "correct user" do
      let(:expected_result) { redirect_to(schedule) }
    end

    it "should update the saved schedule" do
      test_sign_in(user)
      action
      schedule.reload
      expect(schedule.title).to eq("New Title")
    end
  end

  describe "DELETE 'destroy'" do
    let!(:user) { Factory(:user) }
    let!(:schedule) { Factory(:schedule, :user => user) }
    let(:action) { delete 'destroy', :id => schedule }

    it_should_require "sign in" do
      let(:expected_result) { redirect_to(:schedules)}
    end

    it_should_require "correct user" do
      let(:expected_result) { redirect_to(:schedules) }
    end

    it "should delete the schedule" do
      test_sign_in(user)
      expect{action}.to change{Schedule.count}.by(-1)
    end
  end
end
