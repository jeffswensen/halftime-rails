require 'spec_helper'

describe SessionsController do
  describe "GET 'new'" do
    let(:action) { get :new }
    it "should be successful" do
      expect(action).to be_success
    end

    it "should set the @title variable to 'Sign in'" do
      action
      expect(assigns(:title)).to eq("Sign in")
    end
  end

  describe "POST 'create'" do
    describe "with valid email and password" do
      let(:user) { Factory(:user) }
      let(:attr) {
        { :email => user.email, :password => user.password }
      }
      let(:action) { post :create, :session => attr }

      it "should sign the user in" do
        action
        expect(controller).to be_signed_in
      end

      it "should set the 'current_user'" do
        action
        expect(controller.current_user).to eq(user)
      end

      it "should redirect to the user show page" do
        expect(action).to redirect_to(user_path(user))
      end
    end

    describe "invalid signin" do
      let(:attr) {
        { :email => "email@example.com", :password => "invalid" }
      }
      let(:action) { post :create, :session => attr }

      it "should re-render the new page" do
        expect(action).to render_template('new')
      end

      it "should set the @title variable to 'Sign in'" do
        action
        expect(assigns(:title)).to eq("Sign in")
      end

      it "should have a flash.now message" do
        action
        expect(flash.now[:error]).to match(/invalid/i)
      end
    end
  end

  describe "DELETE 'destroy'" do
    it "should sign a user out" do
      test_sign_in(Factory(:user))
      delete :destroy
      expect(controller).to_not be_signed_in
    end

    it "should redirect to root_path" do
      test_sign_in(Factory(:user))
      delete :destroy
      expect(response).to redirect_to(root_path)
    end
  end
end
