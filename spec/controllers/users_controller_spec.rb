require 'spec_helper'

describe UsersController do
  describe "GET 'index'" do
    let(:action) { get :index }
    describe "for non-signed-in users" do
      it "should redirect to sign in" do
        expect(action).to redirect_to(signin_path)
      end

      it "should set flash to 'sign in'" do
        action
        expect(flash[:notice]).to match(/sign in/i)
      end
    end

    describe "for signed-in users" do
      describe "for admins" do
        let(:user) { Factory(:user) }
        let(:second) { Factory(:user, :name => "Bob", :email => "another@example.com") }
        let(:third) { Factory(:user, :name => "Ben", :email => "another@example.net") }
        let(:users) { [user, second, third] }

        before(:each) do
          user.toggle!(:admin)
          test_sign_in(user)
          30.times do
            users << Factory(:user, :email => Factory.next(:email))
          end
        end

        it "should be successful" do
          expect(action).to be_success
        end

        it "should set the @title variable to 'All users'" do
          action
          expect(assigns(:title)).to eq("All users")
        end

        it "should populate the @users variable with existing users" do
          action
          users[0..2].each do |u|
            expect(assigns(:users)).to include(u)
          end
        end
      end

      describe "for regular users" do
        it "should not be successful" do
          @regular_user = Factory(:user)
          test_sign_in(@regular_user)
          expect(action).to redirect_to(root_path)
        end
      end
    end
  end

  describe "GET 'show'" do
    let!(:user) { Factory(:user) }
    let(:action) { get :show, :id => user }
    describe "for non signed-in users" do

      it "should redirect to signin" do
        expect(action).to redirect_to(signin_path)
      end
    end

    describe "for signed-in users" do
     describe "for admins" do
       before(:each) do
         user.toggle!(:admin)
         test_sign_in(user)
       end

        it "should be successful" do
          expect(action).to be_success
        end

        it "should set the @user variable" do
          action
          expect(assigns(:user)).to eq(user)
        end

        it "should populate the @title variable" do
          action
          expect(assigns(:title)).to eq(user.name)
        end

        it "should show the user's schedules" do
          schedule1 = Factory.create(:schedule, :user => user, :title => "Test Title 1")
          schedule2 = Factory.create(:schedule, :user => user, :title => "Test Title 2")

          action
          expect(assigns(:schedules)).to include(schedule1)
          expect(assigns(:schedules)).to include(schedule2)
        end
      end
    end

  end

  describe "GET 'new'" do
    let(:action) { get 'new' }
    it "should be successful" do
      expect(action).to be_success
    end

    it "should set the @title variable" do
      action
      expect(assigns(:title)).to eq("sign up")
    end
  end

  describe "POST 'create'" do

    describe "failure" do
      let(:attr) {
        {:name => "",
        :email => "",
        :password => "",
        :password_confirmation => ""}
      }
      let(:action) { post :create, :user => attr }

      it "should not create a user" do
        expect{action}.to_not change{User.count}
      end

      it "should render the 'new' page" do
        expect(action).to render_template('new')
      end
    end

    describe "success" do
      let(:attr) {
        { :name => "New User", :email => "user@example.com",
          :password => "foobar", :password_confirmation => "foobar" }
      }
      let(:action) { post :create, :user => attr }

      it "should create a user" do
        expect{action}.to change{User.count}.by(1)
      end

      it "should redirect to the user show page" do
        action
        expect{response}.to redirect_to(assigns(:user))
      end

      it "should sign the user in" do
        action
        expect(controller).to be_signed_in
      end
    end
  end

  describe "GET 'edit'" do
    let(:user) { Factory(:user) }
    let(:action) { get :edit, :id => user }

    it_should_require "sign in" do
      let(:expected_result) { be_success }
    end

    it_should_require "correct user" do
      let(:expected_result) { be_success }
    end

    it "should set the @title variable" do
      test_sign_in(user)
      action
      expect(assigns(:title)).to eq("Edit user")
    end
  end

  describe "PUT 'update'" do
    let!(:user) { Factory(:user) }
    let(:attr) {
      {:name => "NewName"}
    }
    let(:action) { post :update, :id => user.id, :user => attr}

    it_should_require "sign in" do
      let(:expected_result) { redirect_to(user) }
    end

    it_should_require "correct user" do
      let(:expected_result) { redirect_to(user) }
    end

    it "should update the saved user" do
      test_sign_in(user)
      action
      user.reload
      expect(user.name).to eq("NewName")
    end

    it "should redirect to the user page" do
      test_sign_in(user)
      action
      expect(response).to redirect_to(user)
    end

  end

  describe "DELETE 'destroy'" do
    let(:user) { Factory(:user) }
    let(:action) { delete 'destroy', :id => user }
    it_should_require "sign in" do
      let(:expected_result) { redirect_to(root_path) }
    end

    it_should_require "correct user" do
      let(:expected_result) { redirect_to(root_path) }
    end
  end
end
