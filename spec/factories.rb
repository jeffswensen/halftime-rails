
# By using the symbol ':user', we get Factory Girl to simulate the User model.
Factory.define :user do |user|
  user.name                  "Michael Hartl"
  user.email                 { Factory.next :email }
  user.password              "foobar"
  user.password_confirmation "foobar"
end

Factory.sequence :email do |n|
  "person-#{n}@example.com"
end

Factory.define :schedule do |schedule|
  schedule.title "Example Schedule"
  schedule.association :user
end

Factory.define :event do |event|
  event.description "Example Event"
  event.occursAt DateTime.now
  event.association :schedule
end