require 'spec_helper'

describe Event do
  let(:user) { Factory(:user) }
  let(:schedule) { Factory(:schedule, :user => user) }
  let(:attr) { {
      :description => "Example Event Description",
      :occursAt => DateTime.now
  } }

  it "should create a new event given valid attributes" do
    expect(schedule.events.create!(attr)).to be_valid
  end

  describe "schedule associations" do
    let(:event) { schedule.events.create!(attr) }


    it "should have a schedule attribute" do
      expect(event).to respond_to(:schedule)
    end

    it "should have the right associated schedule" do
      expect(event.schedule_id).to eq(schedule.id)
      expect(event.schedule).to eq(schedule)
    end
  end

  describe "validations" do

    it "should require a schedule id" do
      expect(Event.new(attr)).to_not be_valid
    end

    it "should require a non-blank description" do
      expect(schedule.events.build(attr.merge(:description => "  "))).to_not be_valid
    end

    it "should require occursAt" do
      expect(schedule.events.build(attr.merge(:occursAt => nil))).to_not be_valid
    end
  end
end
