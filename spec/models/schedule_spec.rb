require 'spec_helper'

describe Schedule do
  let(:user) { Factory(:user) }
  let(:attr) { {:title => "Example Schedule Title" } }

  it "should create a new instance given valid attributes" do
    expect(user.schedules.create!(attr)).to be_valid
  end

  describe "user associations" do
    let(:schedule) { user.schedules.create(attr) }

    it "should have a user attribute" do
      expect(schedule).to respond_to(:user)
    end

    it "should have the right associated user" do
      expect(schedule.user_id).to eq(user.id)
      expect(schedule.user).to eq(user)
    end
  end

  describe "validations" do

    it "should require a user id" do
      expect(Schedule.new(attr)).to_not be_valid
    end

    it "should require nonblank title" do
      expect(user.schedules.build(:title => "  ")).to_not be_valid
    end

    it "should reject long content" do
      expect(user.schedules.build(:title => "a" * 141)).to_not be_valid
    end
  end

  describe "event associations" do
    let(:schedule) { user.schedules.create!(attr) }
    let!(:event1) { Factory(:event, :schedule => schedule, :occursAt => 1.day.ago) }
    let!(:event2) { Factory(:event, :schedule => schedule, :occursAt => 1.hour.ago) }

     it "should have an events attribute" do
       expect(schedule).to respond_to(:events)
     end

     it "should have the right events in the right order" do
       expect(schedule.events).to eq([event1, event2])
     end

     it "should destroy associated events" do
       schedule.destroy
       [event1, event2].each do |event|
         expect(Event.find_by_id(event.id)).to be_nil
       end
     end
   end
end
