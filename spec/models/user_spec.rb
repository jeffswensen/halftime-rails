# == Schema Information
#
# Table name: users
#
#  id         :integer         not null, primary key
#  name       :string(255)
#  email      :string(255)
#  created_at :datetime
#  updated_at :datetime
#

require 'spec_helper'

describe User do
  let(:attr) {
    {
        :name => "Example User",
        :email => "user@example.com",
        :password => "foobar",
        :password_confirmation => "foobar" }
  }

  it "should create a new instance given valid attributes" do
    expect(User.create!(attr)).to be_valid
  end

  it "should require a name" do
    expect(User.new(attr.merge(:name => ""))).to_not be_valid
  end

  it "should reject names that are too long" do
    expect(User.new(attr.merge(:name => 'a' * 51))).to_not be_valid
  end

  it "should require an email address" do
    expect(User.new(attr.merge(:email => ""))).to_not be_valid
  end

  it "should accept valid email addresses" do
    addresses = %w[user@foo.com THE_USER@foo.bar.org first.last@foo.jp]
    addresses.each do |address|
      expect(User.new(attr.merge(:email => address))).to be_valid
    end
  end

  it "should reject invalid email addresses" do
    addresses = %w[user@foo,com user_at_foo.org example.user@foo.]
    addresses.each do |address|
      expect(User.new(attr.merge(:email => address))).to_not be_valid
    end
  end

  it "should reject duplicate email addresses" do
    # Put a user with given email into the db.
    User.create!(attr)
    expect(User.new(attr)).to_not be_valid
  end

  it "should reject email addresses identical upper case" do
    User.create!(attr.merge(:email => attr[:email].upcase))
    expect(User.new(attr.merge(:email => attr[:email].upcase))).to_not be_valid
  end

  describe "admin attribute" do
    let(:user) { User.create!(attr) }

    it "should respond to the admin" do
      expect(user).to respond_to(:admin)
    end

    it "should not be an admin by default" do
      expect(user).to_not be_admin
    end

    it "should be convertible to an admin" do
      user.toggle!(:admin)
      expect(user).to be_admin
    end
  end

  describe "password validations" do

    it "should require a password" do
      expect(User.new(attr.merge(:password => "", :password_confirmation => ""))).to_not be_valid
    end

    it "should require a matching password confirmation" do
      expect(User.new(attr.merge(:password_confirmation => "invalid"))).to_not be_valid
    end

    it "should reject short passwords" do
      short = 'a' * 5
      hash = attr.merge(:password => short, :password_confirmation => short)
      expect(User.new(hash)).to_not be_valid
    end

    it "should reject long passwords" do
      long = "a" * 41
      hash = attr.merge(:password => long, :password_confirmation => long)
      expect(User.new(hash)).to_not be_valid
    end
  end

  describe "password encryption" do
    let!(:user) { User.create!(attr) }

     it "should have an encrypted password attribute" do
       expect(user).to respond_to(:encrypted_password)
     end

     it "should set the encrypted password" do
       expect(user.encrypted_password).to_not be_blank
     end

     it "should be true if the passwords match" do
       expect(user.has_password?(attr[:password])).to be_true
     end

     it "should be false if the passwords don't match" do
       expect(user.has_password?("invalid")).to be_false
     end

     describe "authenticate method" do
       it "should return nil on email/password mismatch" do
         expect(User.authenticate(attr[:email], "wrongpass")).to be_nil
       end

       it "should return nil for an email address with no user" do
         expect(User.authenticate("bar@foo.com", attr[:password])).to be_nil
       end

       it "should return the user on email/password match" do
         expect(User.authenticate(attr[:email], attr[:password])).to eq(user)
       end
     end
   end

   describe "schedule associations" do
     let(:user) { User.create(attr) }
     let!(:schedule1) { Factory(:schedule, :user => user, :created_at => 1.day.ago) }
     let!(:schedule2) { Factory(:schedule, :user => user, :created_at => 1.hour.ago) }

     it "should have a schedules attribute" do
       expect(user).to respond_to(:schedules)
     end

     it "should have the right schedules in the right order" do
       expect(user.schedules).to eq([schedule2, schedule1])
     end

     it "should destroy associated schedules" do
       user.destroy
       [schedule1, schedule2].each do |schedule|
         expect(Schedule.find_by_id(schedule.id)).to be_nil
       end
     end
   end
end
