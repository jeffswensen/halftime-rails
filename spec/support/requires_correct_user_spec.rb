shared_examples_for "correct user" do
  describe "for the wrong user" do
    it "should redirect to root" do
      @wrongUser = Factory(:user)
      test_sign_in(@wrongUser)
      action
      expect{response}.to redirect_to(root_path)
    end
  end
  describe "for the correct user" do
    it "should be successful" do
      test_sign_in(user)
      action
      expect(response).to expected_result
    end
  end
end