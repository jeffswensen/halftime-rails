shared_examples_for "sign in" do
  describe "for non-signed in users" do
    it "should redirect to login page" do
      action
      expect(response).to redirect_to(signin_path)
    end

    it "should request a sign in" do
      action
      expect(flash[:notice]).to match(/sign in/i)
    end
  end

  describe "for signed-in users" do
    it "should be successful" do
      test_sign_in(user)
      action
      expect(response).to expected_result
    end
  end
end